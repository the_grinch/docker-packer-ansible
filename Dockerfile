FROM hashicorp/packer:full-1.6.5

# Define our application versions
ENV ANSIBLE_VERSION 2.9.12
ENV CONSUL_VERSION 1.8.5
ENV TERRAFORM_VERSION 0.13.5

# update/install a dependencies for Ansible, and Consul/Terraform
RUN apk update
RUN apk add --update build-base python3-dev curl openssl-dev libffi-dev jq unzip openssh-client
RUN curl -O https://bootstrap.pypa.io/get-pip.py \
 && python3 get-pip.py
RUN pip install ansible>=${ANSIBLE_VERSION}
RUN pip install https://releases.ansible.com/ansible-tower/cli/ansible-tower-cli-latest.tar.gz
RUN curl -O https://releases.hashicorp.com/consul/${CONSUL_VERSION}/consul_${CONSUL_VERSION}_linux_amd64.zip
RUN unzip consul_${CONSUL_VERSION}_linux_amd64.zip && chmod +x consul && mv consul /usr/local/bin/
RUN curl -O https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip
RUN unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip && chmod +x terraform && mv terraform /usr/local/bin/
# RUN pip install --ignore-installed "pywinrm>=0.3.0"
# RUN curl -fL https://getcli.jfrog.io | sh

# Set a default working dir (nice for bind mounting things inside)
RUN mkdir /build
WORKDIR /build

# Packer needs this set:
# https://github.com/mitchellh/packer/blob/49067e732a66c9f7a87843a2c91100de112b21cc/provisioner/ansible/provisioner.go#L127
ENV USER root

# Set our entrypoint back to the default (gitlab-runner needs this)
ENTRYPOINT ["/bin/sh", "-c"]